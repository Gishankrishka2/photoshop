/*
The objective of this file is check differents requirements
of Photoshop v 21.0 based on his official page: 
https://helpx.adobe.com/es/photoshop/system-requirements/2020.html
*/
#include "Photoshop.h"
#include "Requirements.h"
#include <stdio.h>
// Use this for make syscalls
#include <sys/sysinfo.h>
// Use this for atoi, conversor string to int, strcmp...
#include <string.h>

int check_ram();
int check_cpu();
int check_screen();
int check_gpu();
void print_red();
void print_green();
void print_reset();

int counter = 0;

void check_requirements()
{
    printf("Checking RAM...\n");
    check_ram();
    printf("Checking CPU...\n");
    check_cpu();
    printf("Check screen...\n");
    check_screen();
    printf("Checking GPU...\n");
    check_gpu();
    // Finally checking the space on disk by ask
    char answer;
    printf("You have free more of 3GB on disk?[n,y]: ");
    getchar();
    scanf("%c", &answer);
    if (answer == 'y')
    {
        print_green();
        printf("\xE2\x9C\x93 You have free space available\n");
    }
    else
    {
        print_red();
        printf("X You can't install Photoshop\n");
        counter++;
    }
    getchar();
    print_reset();
    // Finally say if it is compatible or not 
    printf("SUMMARY: \n");
    if (counter != 0)
    {
        print_red();
        printf("X You do not meet the minimum requirements to install Photoshop X \n");
    }
    else
    {
        print_green();
        printf("\xE2\x9C\x93 You are ready for install Photoshop \xE2\x9C\x93 \n");
    }
    print_reset();
    return_program();
}

int check_ram()
{
    // Creating the struct of sys
    struct sysinfo s_info;
    // Check is created correctly
    if (sysinfo(&s_info) == 0)
    {
        // Save RAM in MB
        long int ram = s_info.totalram / 1024 / 1024;
        // Compare with the minimum specs of Photoshop
        if (ram > 1950)
        {
            print_green();
            printf("\xE2\x9C\x93 You have %liMB of 2000MB\n", ram);
        }
        else
        {
            print_red();
            printf("X You don't meet the requirements of RAM %li and need 2GB\n", ram);
            counter++;
        }
    }
    print_reset();
    return 0;
}

int check_cpu()
{
    // Check the architecture of CPU and the GHz
    // Read the output of the command with a pipe
    FILE *cpu_info = popen("getconf LONG_BIT", "r");
    if (cpu_info == NULL)
    {
        fprintf(stderr, "Can't execute command.");
    }
    // Check if the pointer works
    if (cpu_info == NULL)
    {
        fprintf(stderr, "Could not open pipe.\n");
        return 1;
    }
    char architecture[3];
    // Read the output
    fgets(architecture, 3, cpu_info);
    // Check if the architecture is 64 bits
    if (atoi(architecture) == 64)
    {
        print_green();
        printf("\xE2\x9C\x93 You have a 64 bits CPU\n");
        print_reset();
    }
    else
    {
        print_red();
        printf("X You need a 64 bits CPU\n");
        counter++;
    }
    pclose(cpu_info);

    // Read the GHz of the CPU
    cpu_info = popen("grep 'cpu MHz' /proc/cpuinfo | head -1 | awk -F: '{print $2/1024}'", "r");
    // Check if the pointer works
    if (cpu_info == NULL)
    {
        fprintf(stderr, "Could not open pipe.\n");
        return 1;
    }
    char cpu_ghz[7];
    // Read the output
    fgets(cpu_ghz, 7, cpu_info);
    // Convert the string into int (don't need know the decimals)
    if (atoi(cpu_ghz) >= 2)
    {
        print_green();
        printf("\xE2\x9C\x93 You have 2GHz or more of CPU\n");
    }
    else
    {
        print_red();
        printf("X You don't have a 2GHz or more of CPU\n");
        counter++;
    }
    pclose(cpu_info);
    print_reset();
    return 0;
}

int check_screen()
{
    // Screen Resolution
    FILE *screen_info = popen("xdpyinfo | awk '/dimensions/{print $2}'", "r");
    // Check if the pointer works
    if (screen_info == NULL)
    {
        fprintf(stderr, "Could not open pipe.\n");
        return 1;
    }
    char screen[10];
    // Read the output
    fgets(screen, 10, screen_info);
    // Split the height and the width for check
    int i = 0;
    char *split = strtok(screen, "x");
    char *resolution[2];

    while (split != NULL)
    {
        resolution[i++] = split;
        split = strtok(NULL, "x");
    }
    // Convert the string to int and compare the output with the minimum height and width
    if (atoi(resolution[0]) >= 1280 && atoi(resolution[1]) >= 800)
    {
        print_green();
        printf("\xE2\x9C\x93 You have a good resolution\n");
    }
    else
    {
        print_red();
        printf("X You need more resolution\n");
        counter++;
    }
    pclose(screen_info);
    print_reset();
    return 0;
}

int check_gpu()
{
    // Request the vram in MB
    FILE *vram = popen("glxinfo | egrep -i 'video memory:' | head -1 | cut -d ':' -f2", "r");
    // Check if the pointer works
    if (vram == NULL)
    {
        fprintf(stderr, "Could not open pipe.\n");
        return 1;
    }
    // Store VRAM
    char *n_vram[11];
    fgets(n_vram, 11, vram);
    // Check if have enought VRAM
    if (atoi(n_vram) >= 512)
    {
        print_green();
        printf("\xE2\x9C\x93 You have enoguht VRAM\n");
    }
    else
    {
        print_red();
        printf("X You don't have enough VRAM\n");
        counter++;
    }
    pclose(vram);
    print_reset();

    char answer;
    getchar();
    printf("You have an nVidia GeForce GTX 1050 or equivalent or better?[n,y]: ");
    scanf("%c", &answer);
    if (answer == 'y')
    {
        print_green();
        printf("\xE2\x9C\x93 Can install Photoshop\n");
    }
    else
    {
        print_red();
        printf("X You can't install Photoshop\n");
        counter++;
    }
    print_reset();
    return 0;
}

void print_red() 
{
    printf("\033[1;31m");
}

void print_reset() 
{
    printf("\033[0m");
}

void print_green()
{
    printf("\033[0;32m");
}