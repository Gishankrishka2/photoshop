// The objective of this file is to install Adobe Camera Raw v12
#include "Photoshop.h"
#include "Uninstaller.h"

#include <stdio.h>
// Use malloc and exit
#include <stdlib.h>
// Use this for make syscalls
#include <sys/sysinfo.h>
// Use this for atoi, conversor string to int, strcmp...
#include <string.h>

void complete_uninstall(char *location);
void partial_uninstall(char *location);

int uninstall_photoshop()
{
    printf("Welcome to the uninstaller of Photoshop v21\n");
    printf("Please, remember me some questions for a more personalized uninstall:\n");
    // Ask if he made a installation in all system wide or only in his user
    char *answer = malloc(sizeof(char) * 5);
    if (answer == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    printf("You have installed Photoshop in the whole system or only in your user??[whole, only]: ");
    getchar();
    scanf("%s", answer);
    // Delete icon
    system("sudo rm /usr/share/icons/hicolor/256x256/apps/photoshop-cc.png");
    // Deleting .desktop
    if (answer[0] == 'o')
    {
        system("rm $HOME/.local/share/applications/photoshop.desktop");
    }
    else if (answer[0] == 'w')
    {
        system("sudo rm /usr/share/applications/photoshop.desktop");
    }
    free(answer);
    // Ask for installation location
    char *location = malloc(sizeof(char) * 150);
    if (location == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    printf("What is the installation path that you have written in the installer? (Absolute Path):\n");
    getchar();
    scanf("%s", location);
    // Finally ask if want uninstall all, including Wine and winetricks
    answer = malloc(sizeof(char) * 5);
    if (answer == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    printf("You want uninstall all (Wine, winetricks and Photoshop files)\n or only all Photoshop files (included Camera Raw)?[all, only]: ");
    getchar();
    scanf("%s", answer);
    if (answer[0] == 'a')
    {
        printf("Starting complete uninstaller...");
        complete_uninstall(location);
    }
    else if (answer[0] == 'o')
    {
        printf("Starting partial uninstaller...");
        partial_uninstall(location);
    }
    free(answer);
    free(location);
    return_program();
}

void complete_uninstall(char *location)
{
    // First check what distribution is
    char *distribution = malloc(sizeof(char) * 4);
    printf("Do you have an Debian or Ubuntu or any based distro? [yes/no]: ");
    scanf("%s", distribution);
    // Uninstall dependencies
    if (distribution[0] == 'y')
    {
        // Uninstalling packages is the same in Debian that in Ubuntu
        system("sudo apt-get remove wine-stable wine-stable-amd64 ");
    }
    else
    {
        printf("Do you have an Arch Linux or any based distro? [yes/no]: ");
        scanf("%s", distribution);
        if (distribution[0] == 'y')
        {
            system("sudo pacman -Rsn wine");
        }
        else
        {
            printf("Do you have a Fedora distro?: [yes/no]: ");
            scanf("%s", distribution);
            if (distribution[0] == 'y')
            {
                system("sudo dnf remove winehq-stable");
            }
            else
            {
                printf("The distribution you are using is not supported or cannot be recognized");
                free(distribution);
                return 1;
            }
        }
        // Free memory of distribution
        free(distribution);
        // Uninstall winetricks
        system("sudo rm -r /usr/local/bin/winetricks");
        // Now remove all files of Photoshop
        partial_uninstall(location);
    };
};

void partial_uninstall(char *location)
{
    // Delete only the Photoshop folder
    char *command = malloc(sizeof(char) * 250);
    sprintf(command, "sudo rm -r %s/PhotoshopCC", location);
    system(command);
};